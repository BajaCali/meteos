//
//  MassFormatter.swift
//  Meteos
//
//  Created by Michal on 23.06.22.
//

import Foundation

 struct MassFormatStyle: FormatStyle {
    func format(_ value: Double) -> String {
        guard value > 0.5 else {
            return "<1 g"
        }

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1

        let displayValue: Double
        let units: String
        if value > ViewConstants.MassThreshold.large.maxMassValue {
            displayValue = value / 1000
            units = "kg"
        } else {
            displayValue = value
            units = "g"
        }

        let nsNumber = NSNumber(value: displayValue)
        return formatter.string(from: nsNumber)! + " " + units
    }
 }
