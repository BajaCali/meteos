//
//  FileManager+JSON.swift
//  Meteos
//
//  Created by Michal on 13.06.22.
//

import Foundation

public protocol JSONEncoding {
    func encode<T>(_ value: T) throws -> Data where T: Encodable
}

extension JSONEncoder: JSONEncoding { }

public protocol JSONDecoding {
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T: Decodable
}

extension JSONDecoder: JSONDecoding { }

public protocol AppDocumentsFolderURLProviding {
    var documentsFolder: URL { get }
}

extension FileManager: AppDocumentsFolderURLProviding {
    public var documentsFolder: URL {
        self.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}

/// Saves given variable transformable to JSON (`jsonData`) into file named `filename` in application's user document
/// folder.
///
/// Saving to this folder means that data:
/// - are not visible to the user,
/// - can be backed up via iCloud without any further code,
/// - when uninstalling user get prompt (an alert) whether to delete the data too.
///
/// - Parameters:
///   - jsonData: variable to be saved
///   - filename: name of file where to save
///   - fileManager: optional specification of file manager to get documents folder
///   - jsonEncoder: optional encoder to json
/// - Throws: May fail to encode variable or save data.
public func saveAsJSONToDocuments<T>(
    _ jsonData: T,
    filename: String,
    on fileManager: AppDocumentsFolderURLProviding = FileManager.default,
    with jsonEncoder: JSONEncoding = JSONEncoder()
) throws where T: Encodable {
    let fileUrl = fileManager.documentsFolder
        .appendingPathComponent(filename)

    let data = try jsonEncoder.encode(jsonData)
    try data.write(to: fileUrl)
}

/// Reads from application's user documents folder file named `filename` and tries to decode it as `type`.
///
/// - Parameters:
///   - type: give type youre are expecting to be in in the file in JSON format
///   - filename: name of file which to read from
///   - fileManager: optional specification of file manager to get documents folder
///   - jsonDecoder: optional decoder from json
/// - Throws: May fail to read from file (e.g. when the file is not present) or to decode file content to given type.
/// - Returns: Your variable saved in the given file named `filename` as given `type`.
public func readJSONFromDocuments<T>(
    as type: T.Type,
    filename: String,
    on fileManager: AppDocumentsFolderURLProviding = FileManager.default,
    with jsonDecoder: JSONDecoding = JSONDecoder()
) throws -> T where T: Decodable {
    let fileUrl = fileManager.documentsFolder
        .appendingPathComponent(filename)

    let data = try Data(contentsOf: fileUrl)
    return try jsonDecoder.decode(T.self, from: data)
}
