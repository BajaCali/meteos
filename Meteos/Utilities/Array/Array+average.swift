//
//  Array+average.swift
//  Meteos
//
//  Created by Michal on 22.06.22.
//

import Foundation

extension Array where Element == Double {
    func average() -> Element? {
        guard isNotEmpty else { return nil}

        return self.reduce(0, +) / Double(count)
    }
}
