//
//  LandingArray+asDictionary.swift
//  Meteos
//
//  Created by Michal on 25.06.22.
//

import Foundation

extension Array where Element == MeteoriteLanding {
    func asDictionary() -> [MeteoriteLandingID: MeteoriteLanding] {
        self .reduce(into: [MeteoriteLandingID: MeteoriteLanding]()) { accumulated, landing in
            accumulated[landing.identification] = landing
        }
    }
}
