//
//  Array+isNotEmpty.swift
//  Meteos
//
//  Created by Michal on 22.06.22.
//

import Foundation

extension Collection {
    var isNotEmpty: Bool {
        !self.isEmpty
    }
}
