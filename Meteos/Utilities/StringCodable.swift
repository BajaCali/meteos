//
//  StringCodable.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

// inspiration: https://forums.swift.org/t/converting-numbers-in-string-to-int/38566/4
@propertyWrapper
struct StringCodable<Wrapped: LosslessStringConvertible>: Codable {
    let wrappedValue: Wrapped

    init(wrappedValue: Wrapped) {
        self.wrappedValue = wrappedValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let string = try container.decode(String.self)
        if let value = Wrapped(string) {
            wrappedValue = value
        } else {
            throw DecodingError.dataCorruptedError(
                in: container,
                debugDescription: "Value \(string) is not convertible to \(Wrapped.self)"
            )
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(wrappedValue.description)
    }
}

extension StringCodable: Equatable
where Wrapped: Equatable {
    // swiftlint:disable:next identifier_name
    static func == (lhs: StringCodable<Wrapped>, rhs: StringCodable<Wrapped>) -> Bool {
        lhs.wrappedValue == rhs.wrappedValue
    }
}
