//
//  Optional+otherwise.swift
//  Meteos
//
//  Created by Michal on 25.06.22.
//

import Foundation

extension Optional {
    func otherwise(_ ifNil: Wrapped) -> Wrapped {
        switch self {
        case .none:
            return ifNil
        case .some(let wrapped):
            return wrapped
        }
    }
}
