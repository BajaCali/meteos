//
//  Router.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

// MARK: - Cases
enum Router {
    case landings(year: Int)
}

// MARK: - Endpoint properties
extension Router: Endpoint {
    var path: String {
       "resource/y77d-th95.json"
    }

    var method: HTTPMethod {
        .get
    }

    var urlParameters: [(String, Any)]? {
        var parameters = [(String, Any)]()

        switch self {
        case let .landings(year):
            parameters.append(("year", "\(year)-01-01T00:00:00.000"))
        }

        return parameters
    }

    var headers: [String: String]? {
        var headers = [String: String]()
        headers["X-App-Token"] = Constants.apiToken
        return headers
    }

}
