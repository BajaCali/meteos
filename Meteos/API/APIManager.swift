//
//  APIManager.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

// MARK: - Protocol
protocol APIManaging {
    func request<T: Decodable>(_ endpoint: Endpoint) async throws -> T
}

// MARK: - Private Implementation
final class APIManager {
    private lazy var urlSession: URLSession = {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        config.timeoutIntervalForResource = 30

        return URLSession(configuration: config)
    }()

    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()

        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
//                             "1939-01-01'T'00:00:00.000
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        decoder.dateDecodingStrategy = .formatted(formatter)

        return decoder
    }()

    private func request(_ endpoint: Endpoint) async throws -> Data {
        let request: URLRequest = try endpoint.asRequest()

        let (data, response) = try await urlSession.data(for: request)

        guard let httpResponse = response as? HTTPURLResponse else {
            throw APIError.noResponse
        }

        guard 200..<300 ~= httpResponse.statusCode else {
            throw APIError.unacceptableResponseStatusCode
        }

        return data
    }
}

// MARK: - Conformances
extension APIManager: APIManaging {
    func request<T: Decodable>(_ endpoint: Endpoint) async throws -> T {
        let data = try await request(endpoint)
        return try decoder.decode(T.self, from: data)
    }
}
