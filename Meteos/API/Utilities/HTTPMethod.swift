//
//  HTTPMethod.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation
enum HTTPMethod: String {
    // swiftlint:disable:next identifier_name
    case get = "GET"
}
