//
//  APIError.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

enum APIError: String, Error {
    case invalidUrlComponents
    case noResponse
    case unacceptableResponseStatusCode
    case customDecodingFailed
    case malformedUrl
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        return self.rawValue
    }
}
