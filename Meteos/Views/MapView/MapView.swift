//
//  MapView.swift
//  Meteos
//
//  Created by Michal on 21.06.22.
//

import SwiftUI
import MapKit

struct MapView: View {
    @StateObject private var viewModel: ViewModel

    init() {
        _viewModel = StateObject(wrappedValue: ViewModel())
    }

    // MARK: - Body
    var body: some View {
        Group {
            switch viewModel.state {
            case .initial, .fetching:
                ProgressView { Text("Loading map...") }
            case .failed:
                StandardRowView( annotation: "Sorry, failed to load. 😔", value: "" )
                    .padding()
            case .success:
                landingsMap
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(ViewConstants.backgroundColor)
    }

    // MARK: - Sub-Views
    private var landingsMap: some View {
        Map(
            coordinateRegion: $viewModel.mapRegion,
            showsUserLocation: true,
            annotationItems: landingsKeys,
            annotationContent: { landingID in
                MapAnnotation(coordinate: landings[landingID]?.location ?? Constants.defaultLocation) {
                    let landing = landings[landingID]!
                    LandingMapAnnotation(landing: landing)
                        .onTapGesture {
                            viewModel.selectedLandingID = landing.identification
                        }
                }
        })
        .sheet(item: $viewModel.selectedLandingID) { landingID in
            Sheet(landingID: landingID, viewModel: viewModel)
        }
        .ignoresSafeArea(.all, edges: .top)
    }

    // MARK: - Data
    private var landings: [MeteoriteLandingID: MeteoriteLanding] {
        viewModel.landings
    }

    private var landingsKeys: [MeteoriteLandingID] {
        landings.keys.reduce(into: [MeteoriteLandingID](), { accumulated, landingId in accumulated.append(landingId) })
    }
}

// MARK: - Sheet
extension MapView {
    private struct Sheet: View {
        @Environment(\.dismiss) var dismiss

        let landingID: MeteoriteLandingID
        let viewModel: ViewModel

        var body: some View {
            if let landing = viewModel.landings[landingID] {
                createSheet(for: landing)
            } else {
                EmptyView()
            }
        }

        private func createSheet(for landing: MeteoriteLanding) -> some View {
            NavigationView {
                MeteoriteLandingDetailView(landing: landing)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button {
                                dismiss()
                            } label: {
                                Image(systemName: "xmark.circle")
                            }
                        }
                    }
            }
        }
    }
}

// MARK: - Previews
struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
