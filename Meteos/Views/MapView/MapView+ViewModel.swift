//
//  MapView+ViewModel.swift
//  Meteos
//
//  Created by Michal on 21.06.22.
//

import Foundation
import DependencyInjection
import MapKit

// MARK: - State
extension MapView {
    enum FetchingState {
        case initial
        case fetching
        case failed
        case success
    }
}

// MARK: - Implementation
extension MapView {
    @MainActor
    class ViewModel: ObservableObject {
        // MARK: Published properties
        @Published var mapRegion = MKCoordinateRegion(
            center: Constants.defaultLocation,
            span: MKCoordinateSpan(latitudeDelta: 20, longitudeDelta: 20)
        )
        @Published var landings = [MeteoriteLandingID: MeteoriteLanding]()
        @Published var selectedLandingID: MeteoriteLandingID?
        @Published var state = FetchingState.initial

        // MARK: Dependencies
        @Injected private var locationManager: LocationManager
        @Injected private var landingsStore: LandingsStoring

        // MARK: Fetching and initialisation
        init() {
            initialFetching()
        }

        private func initialFetching() {
            self.state = .fetching
            Task {
                self.state = await withTaskGroup(of: Void.self) { group -> FetchingState in
                    group.addTask { try? await self.fetchLandings() }
                    group.addTask { await self.fetchLocation() }

                    await group.waitForAll()

                    if landings.isNotEmpty {
                        return .success
                    }

                    return .failed
                }
            }
        }

        func fetchLandings() async throws {
            landings = try await landingsStore.getClusters()
                .asDictionary()
        }

        func fetchLocation() async {
            if let location = try? await locationManager.getLocation() {
                mapRegion = MKCoordinateRegion(center: location, span: mapRegion.span)
            }
        }
    }
}
