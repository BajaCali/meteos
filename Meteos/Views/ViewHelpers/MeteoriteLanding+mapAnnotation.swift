//
//  MeteoriteLanding+mapAnnotation.swift
//  Meteos
//
//  Created by Michal on 25.06.22.
//

import SwiftUI

struct LandingMapAnnotation: View {
    let landing: MeteoriteLanding

    var body: some View {
        VStack(spacing: 10 * landing.scale) {
            Image(landing.unwrapped { _ in
                Asset.landing.name
            } cluster: { _ in
                Asset.cluster.name
            })
            .resizable()
            .frame(width: 46, height: 40) // ~ minimal interactive thing size
            .scaled(by: landing)

            Text(landing.name)
                .fixedSize()
        }
    }
}
