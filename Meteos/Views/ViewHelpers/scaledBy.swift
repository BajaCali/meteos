//
//  scaledBy.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import Foundation
import SwiftUI

struct ScaledByLanding: ViewModifier {
    let landing: MeteoriteLanding

    func body(content: Content) -> some View {
        content
            .scaleEffect(landing.scale)
    }
}

extension View {
    func scaled(by landing: MeteoriteLanding) -> some View {
        modifier(ScaledByLanding(landing: landing))
    }
}
