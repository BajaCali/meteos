//
//  ClusterDetailView.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import SwiftUI

private typealias MapViewConstants = MeteoriteLandingDetailView.ViewConstants

struct ClusterDetailView: View {
    let cluster: LandingCluster

    var body: some View {
        ScrollView {
            LazyVStack {
                StandardRowView(withInnerPadding: false) {
                    SimpleMapView(landings: cluster.landings)
                        .frame(height: MapViewConstants.defaultMapViewHeight)
                }

                HorizontalDivider()

                ForEach(0..<rows.count, id: \.self) { index in
                    StandardRowView {
                        rows[index]
                    }
                }

                HorizontalDivider()

                VStack(alignment: .leading) {
                    Text("Landings")
                        .font(.title.bold())
                        .padding(.bottom)

                    ForEach(0..<cluster.landings.count, id: \.self) { index in
                        NavigationLink {
                            MeteoriteLandingDetailView(landing: cluster.landings[index])
                        } label: {
                            MeteoriteLandingRowView(landing: cluster.landings[index] as MeteoriteLanding)
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
            .padding(.horizontal)
        }
        .background(ViewConstants.backgroundColor)
        .navigationTitle(cluster.name)
    }

    private var rows: [AnyView] {
        [
            AnyView(cluster.year.flatMap {
                StandardRowView(annotation: "Year:", value: String($0))
            }),
            AnyView(cluster.averageMass.flatMap {
                StandardRowView(annotation: "Average mass:", value: $0.formatted(MassFormatStyle()))
            }),
            AnyView(cluster.totalMass.flatMap {
                StandardRowView(annotation: "Total mass:", value: $0.formatted(MassFormatStyle()))
            }),
            AnyView(StandardRowView(annotation: "Number of landings:", value: String(cluster.landings.count)))
        ]
    }

}

struct ClusterDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ClusterDetailView(cluster: .example)
                .preferredColorScheme(.dark)
        }
    }
}
