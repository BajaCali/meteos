//
//  MeteoriteLandingDetailView.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import SwiftUI

extension MeteoriteLandingDetailView {
    enum ViewConstants {
        static let defaultMapViewHeight: Double = 200
    }
}

struct MeteoriteLandingDetailView: View {
    let landing: MeteoriteLanding

    var body: some View {
        landing.unwrapped {
            AnyView(LandingDetailView(landing: $0))
        } cluster: {
            AnyView(ClusterDetailView(cluster: $0))
        }
    }
}

struct MeteoriteLandingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MeteoriteLandingDetailView(landing: Landing.example as MeteoriteLanding)
    }
}
