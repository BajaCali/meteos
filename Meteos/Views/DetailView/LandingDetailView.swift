//
//  LandingDetailView.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import SwiftUI

private typealias MapViewConstants = MeteoriteLandingDetailView.ViewConstants

struct LandingDetailView: View {
    let landing: Landing

    var body: some View {
        ScrollView {
            LazyVStack {
                StandardRowView(withInnerPadding: false) {
                    SimpleMapView(landings: [landing])
                        .frame(height: MapViewConstants.defaultMapViewHeight)
                }

                HorizontalDivider()

                ForEach(0..<rows.count, id: \.self) { index in
                    StandardRowView {
                        rows[index]
                    }
                }
            }
            .padding(.horizontal)
        }
        .background(ViewConstants.backgroundColor)
        .navigationTitle(landing.name)
    }

    private var rows: [AnyView] {
        [
            AnyView(nametype),
            AnyView(year),
            AnyView(mass),
            AnyView(recclass)
        ]
    }

    private var nametype: some View {
        StandardRowView(annotation: landing.nametype.rawValue, value: "")
    }

    @ViewBuilder
    private var year: some View {
        if let year = landing.year {
            StandardRowView(annotation: "\(landing.fall.rawValue):", value: String(year))
        } else {
            StandardRowView(annotation: "\(landing.fall.rawValue)", value: "")
        }
    }

    private var mass: some View {
        landing.mass
            .flatMap { $0.formatted(MassFormatStyle()) }
            .map { mass in
                StandardRowView(annotation: "Mass:", value: mass)
            }
    }

    private var recclass: some View {
        StandardRowView(annotation: "Class:", value: landing.recclass)
    }
}

struct LandingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LandingDetailView(landing: .example)
                .preferredColorScheme(.dark)
        }
    }
}
