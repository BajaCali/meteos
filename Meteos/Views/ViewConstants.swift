//
//  ViewConstants.swift
//  Meteos
//
//  Created by Michal on 23.06.22.
//

import Foundation
import SwiftUI

typealias GlobalViewConstants = ViewConstants

enum ViewConstants {
    // MARK: Colours
    static let backgroundColor: Color = Color(red: 0.1, green: 0.1, blue: 0.2)
    static let lightBackgroundColor: Color = Color(red: 0.2, green: 0.2, blue: 0.3)

    // MARK: Mass Threshold
    enum MassThreshold {
        static let small: (maxMassValue: Double, scale: Double) = (1, 0.7)
        static let normal: (maxMassValue: Double, scale: Double) = (200, 1)
        static let large: (maxMassValue: Double, scale: Double) = (5_000, 1.3)
        static let extraLarge: (maxMassValue: Double, scale: Double) = (100_000, 1.7)
    }
}
