//
//  ListView+ViewModel.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation
import DependencyInjection

// MARK: - State
extension ListView {
    enum FetchingState {
        case initial
        case fetching
        case locationFound
        case landingsFetched
        case success
        case failed
    }
}

// MARK: - Implementation
extension ListView {
    @MainActor
    class ViewModel: ObservableObject {
        // MARK: Published
        @Published var landings = [MeteoriteLanding]()
        @Published var location: Location?
        @Published var totalNumberOfLandings: Int = 0
        @Published var state = FetchingState.initial
        @Published var searchText = ""
        @Published var currentSorter = MeteoriteLandingSorter.alphabetical

        // MARK: Dependencies
        @Injected private var landingsStore: LandingsStoring
        @Injected private var locationManager: LocationManager

        private var stateGuard = StateGuard()

        private var allLandings = [MeteoriteLanding]()

        // MARK: Init
        init() {
            initialFetching()
        }

        // MARK: Private functions
        private func initialFetching() {
            state = .fetching
            Task {
                await fetchLandings()
            }
            Task {
                try? await fetchLocation()
            }
        }

        func fetchLandings() async {
            do {
                allLandings = try await landingsStore.getClusters()
                totalNumberOfLandings = try await landingsStore.getLandings().count
            } catch {
                state = await stateGuard.update(with: .failed)
            }
            refreshLandings()
            state = await stateGuard.landingsFetched()
        }

        func fetchLocation() async throws {
            let fetchedLocation = try await locationManager.getLocation()
            self.location = fetchedLocation
            currentSorter = .byDistance(fetchedLocation)
            state = await stateGuard.locationFound()
        }

        func refreshLandings() {
            if location == nil {
                Task {
                    try? await fetchLocation()
                }
            }
            landings = currentSorter.sorted(landings: allLandings)
                .filter { isIncludedInSearch($0) }
        }

        private func isIncludedInSearch(_ landing: MeteoriteLanding) -> Bool {
            guard searchText.isNotEmpty else { return true }
            if landing.name.localizedCaseInsensitiveContains(searchText) {
                return true
            }
            if let landing = landing as? Landing,
               landing.recclass.localizedCaseInsensitiveContains(searchText) {
                return true
            }
            return false
        }
    }
}

// MARK: - Async State Handler
private extension ListView {
    actor StateGuard {
        var state = FetchingState.initial

        private func compareAndSwap(
            test: FetchingState,
            onMatch: FetchingState,
            onFailed: FetchingState? = nil,
            perceiveFailedOnFailed: Bool = true
        ) -> FetchingState {
            if state == test {
                state = onMatch
            } else if !perceiveFailedOnFailed || state != .failed {
                state = onFailed ?? state
            }
            return state
        }

        func update(with state: FetchingState) -> FetchingState {
            self.state = state
            return state
        }

        func locationFound() -> FetchingState {
            self.compareAndSwap(test: .landingsFetched, onMatch: .success)
        }

        func landingsFetched() -> FetchingState {
            self.compareAndSwap(test: .locationFound, onMatch: .success, onFailed: .landingsFetched)
        }
    }
}
