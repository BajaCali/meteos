//
//  MeteoriteLandingSorter.swift
//  Meteos
//
//  Created by Michal on 25.06.22.
//

import Foundation

// MARK: - protocol
protocol Sorting {
    associatedtype Element
    func sorted(landings: [Element]) -> [Element]
}

// MARK: - Cases
enum MeteoriteLandingSorter {
    case alphabetical
    case byDistance(Location)
    case byMassAscending
    case byMassDescending
    case newestFirst
    case oldestFirst
    // TODO: .
    /*
    case byClass
     */
}

// MARK: - CaseIterable
extension MeteoriteLandingSorter: CaseIterable {
    static var allCases: [MeteoriteLandingSorter] {
        [
            .alphabetical,
            .byDistance(Constants.defaultLocation),
            .byMassAscending,
            .byMassDescending,
            .newestFirst,
            .oldestFirst
        ]
    }
}

// MARK: - Equatable
extension MeteoriteLandingSorter: Equatable {
    // swiftlint:disable:next identifier_name
    static func == (lhs: MeteoriteLandingSorter, rhs: MeteoriteLandingSorter) -> Bool {

        return lhs.description == rhs.description
    }
}

// MARK: - CustomStringConvertible
extension MeteoriteLandingSorter: CustomStringConvertible {
    var description: String {
        switch self {
        case .alphabetical:
            return "Alphabetically"
        case .byDistance:
            return "By distance from you"
        case .byMassAscending:
            return "Lighter first"
        case .byMassDescending:
            return "Heavier first"
        case .newestFirst:
            return "Latest first"
        case .oldestFirst:
            return "Older first"
        }
    }
}

// MARK: - Identifiable
extension MeteoriteLandingSorter: Identifiable {
    var id: Int {
        return Self.allCases.firstIndex(of: self)!
    }
}

// MARK: - Sorting
extension MeteoriteLandingSorter: Sorting {
    // swiftlint:disable:next identifier_name
    private func sorter(lhs: MeteoriteLanding, rhs: MeteoriteLanding) -> Bool {
        switch self {
        case .alphabetical:
            return lhs.name < rhs.name
        case .byDistance(let location):
            return optionalSort(lhs.distance(from: location), rhs.distance(from: location))
        case .byMassAscending:
            return optionalSort(lhs.mass, rhs.mass)
        case .byMassDescending:
            return optionalSort(rhs.mass, lhs.mass)
        case .newestFirst:
            return optionalSort(rhs.year, lhs.year)
        case .oldestFirst:
            return optionalSort(lhs.year, rhs.year)
        }
    }

    // swiftlint:disable identifier_name
    private func optionalSort<T: Comparable>(_ lhs: T?, _ rhs: T?) -> Bool {
        guard let lhs = lhs,
              let rhs = rhs else {
            // skip / send lhs back
            return false
        }
        return lhs < rhs
    }
    // swiftlint:enable identifier_name

    func sorted(landings: [MeteoriteLanding]) -> [MeteoriteLanding] {
        landings
            .sorted(
                by: self.sorter,
                MeteoriteLandingSorter.alphabetical.sorter
            )
    }

}

// MARK: - Sort with multiple criteria
// inspiration: https://stackoverflow.com/a/37612765
extension Sequence {
  func sorted(
    by firstPredicate: (Element, Element) -> Bool,
    _ secondPredicate: (Element, Element) -> Bool,
    _ otherPredicates: ((Element, Element) -> Bool)...
  ) -> [Element] {
    // swiftlint:disable:next identifier_name
    return sorted(by:) { lhs, rhs in
      if firstPredicate(lhs, rhs) { return true }
      if firstPredicate(rhs, lhs) { return false }
      if secondPredicate(lhs, rhs) { return true }
      if secondPredicate(rhs, lhs) { return false }
      for predicate in otherPredicates {
        if predicate(lhs, rhs) { return true }
        if predicate(rhs, lhs) { return false }
      }
      return false
    }
  }
}
