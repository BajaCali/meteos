//
//  ListView.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import SwiftUI
import CoreLocation

// MARK: - Constants
extension ListView {
    enum ViewConstants {
        static let spaceBeforeFirstRow: Double = 10
    }
}

// MARK: - Implementation
struct ListView: View {
    @ObservedObject private var viewModel: ViewModel

    init() {
        _viewModel = ObservedObject(wrappedValue: ViewModel())
    }

    // MARK: - Body
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVStack {
                    Color.clear
                        .frame(height: ViewConstants.spaceBeforeFirstRow)

                    switch viewModel.state {
                    case .initial, .fetching, .locationFound:
                       ProgressView()
                    case .success, .landingsFetched:
                        successView
                    case .failed:
                        StandardRowView(annotation: "Sorry, failed to load. 😔", value: "")
                        StandardRowView(annotation: "", value: "Check your Internet connection or restart the app. 😉")
                    }
                }
                .padding(.horizontal)
            }
            .onChange(of: viewModel.searchText) { _ in
                viewModel.refreshLandings()
            }
            .onChange(of: viewModel.currentSorter) { _ in
                viewModel.refreshLandings()
            }
            .searchable(text: $viewModel.searchText)
            .background(GlobalViewConstants.backgroundColor)
            .navigationTitle("Landings")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    SortingMenu(viewModel: viewModel)
                }
            }
        }
    }

    // MARK: - Sub-Views

    @ViewBuilder
    private var successView: some View {
        countInfo
        HorizontalDivider()
        rows

    }
    private var location: some View {
        viewModel.location
            .flatMap { "Location: \($0.latitude) \($0.longitude)"}
            .map { Text($0) }
    }

    @ViewBuilder
    private var countInfo: some View {
        StandardRowView(
            annotation: "Number of landings (clustered):",
            value: String(viewModel.landings.count)
        )

        StandardRowView(
            annotation: "Total number of landings:",
            value: String(viewModel.totalNumberOfLandings)
        )
    }

    private var rows: some View {
        ForEach(viewModel.landings, id: \.identification) { landing in
            NavigationLink {
                MeteoriteLandingDetailView(landing: landing)
            } label: {
                MeteoriteLandingRowView(landing: landing)
            }
            .buttonStyle(PlainButtonStyle())
        }
    }

    private struct SortingMenu: View {
        @ObservedObject var viewModel: ViewModel

        var body: some View {
                Menu {

                    ForEach(MeteoriteLandingSorter.allCases) { sort in
                        if displaySort(sort) {
                            Button {
                                assignSort(sort)
                            } label: {
                                if viewModel.currentSorter == sort {
                                    Label(sort.description, systemImage: "checkmark")
                                } else {
                                    Text(sort.description)
                                }
                            }
                        }
                    }
                } label: {
                    Label("Sort", systemImage: "line.3.horizontal.decrease.circle")
                }
        }

        private func displaySort(_ sort: MeteoriteLandingSorter) -> Bool {
            switch sort {
            case .byDistance:
                return viewModel.location != nil
            default:
                return true
            }
        }

        private func assignSort(_ sort: MeteoriteLandingSorter) {
            switch sort {
            case .byDistance:
                if let location = viewModel.location {
                    viewModel.currentSorter = .byDistance(location)
                    return
                }
                fallthrough
            default:
                viewModel.currentSorter = sort
            }
        }
    }
}

// MARK: - Previews
struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
