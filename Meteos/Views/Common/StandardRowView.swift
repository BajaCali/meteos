//
//  StandardRowView.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import SwiftUI

// MARK: - Constants
private enum StandardRowViewViewConstants {
    static let rowBackgroundColor: Color = GlobalViewConstants.lightBackgroundColor
    static let verticalPadding: Double = 5
    static let horizontalPadding: Double = 10
    static let cornerRadius: Double = 10
}

// MARK: - Implementation
struct StandardRowView<Content: View>: View {
    let withInnerPadding: Bool
    @ViewBuilder let content: () -> Content

    var body: some View {
        HStack {
            content()
                .frame(maxWidth: .infinity)
                .padding(.horizontal, withInnerPadding ? StandardRowViewViewConstants.horizontalPadding : 0)
                .padding(.vertical, withInnerPadding ? StandardRowViewViewConstants.verticalPadding : 0)

        }
        .background(StandardRowViewViewConstants.rowBackgroundColor)
        .clipShape(RoundedRectangle(cornerRadius: StandardRowViewViewConstants.cornerRadius))
    }
}

// MARK: - Convenience
extension StandardRowView {
    init(content: @escaping () -> Content) {
        self.init(withInnerPadding: true, content: content)
    }
}

extension StandardRowView
where Content == HStack<TupleView<(Text, Spacer, Text)>> {
    init(annotation: String, value: String) {
        self.init {
            HStack {
                Text(annotation)
                    .font(.headline)
                Spacer()
                Text(value)
            }
        }
    }
}

// MARK: - Preview
struct StandardRowView_Previews: PreviewProvider {
    static var previews: some View {
        StandardRowView { Text("hello") }
            .preferredColorScheme(.dark)
    }
}
