//
//  HorizontalDivider.swift
//  Meteos
//
//  Created by Michal on 24.06.22.
//

import SwiftUI

struct HorizontalDivider: View {
    var body: some View {
        Rectangle()
            .frame(height: 2)
            .foregroundColor(ViewConstants.lightBackgroundColor)
            .padding(.vertical)
    }
}

struct HorizontalDivider_Previews: PreviewProvider {
    static var previews: some View {
        HorizontalDivider()
    }
}
