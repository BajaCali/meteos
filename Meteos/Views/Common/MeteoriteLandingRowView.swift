//
//  MeteoriteLandingRowView.swift
//  Meteos
//
//  Created by Michal on 23.06.22.
//

import SwiftUI

// MARK: - Constants
fileprivate extension MeteoriteLandingRowView {
    enum ViewConstants {
        // MARK: Dimensions
        static let spacingBetweenLines: Double = 5
        static let defaultIconSize: Double = 32
        static let defaultRowHeight: Double = 40
        static let minimalRowHeight: Double = 44
        static let landingIconTrailingPadding: Double = 10

        // MARK: Highlight Colours
        static let yearHightColor: Color = .blue
        static let recclassHighlightColor: Color = .brown
        static let massHighlightColor: Color = .gray
        static let totalMassHighlightColor: Color = Self.massHighlightColor
        static let averageMassHighlightColor: Color = Self.massHighlightColor

        // MARK: Highlight
        static let highlightCornerRadius: Double = 5
        static let highlightPadding: Double = 2
    }
}

// MARK: - RowView
struct MeteoriteLandingRowView: View {
    let landing: MeteoriteLanding

    var body: some View {
        StandardRowView {
            HStack {
                specificInformation

                Spacer()

                Image(decorative: imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(height: ViewConstants.defaultIconSize)
                    .scaled(by: landing)
                    .padding(.trailing, ViewConstants.landingIconTrailingPadding)
            }
            .frame(height: max(ViewConstants.minimalRowHeight, ViewConstants.defaultRowHeight * landing.scale))
        }
    }

    var imageName: String {
        landing.unwrapped { _ in
            Asset.landing.name
        } cluster: { _ in
            Asset.cluster.name
        }
    }

    var specificInformation: AnyView {
        landing.unwrapped {
            AnyView(SingleLanding(landing: $0))
        } cluster: {
            AnyView(Cluster(cluster: $0))
        }
    }
}

// MARK: - Single landing
extension MeteoriteLandingRowView {
    struct SingleLanding: View {
        let landing: Landing

        var body: some View {
            VStack(alignment: .leading, spacing: ViewConstants.spacingBetweenLines) {
                Text(landing.name)
                    .font(.headline)

                HStack {
                    fallYear
                    landingClass
                    landing.mass
                        .flatMap { $0.formatted(MassFormatStyle()) }
                        .map(Text.init)
                        .map { $0.highlighted(color: ViewConstants.massHighlightColor)}
                }
                .font(.caption.bold())
            }
        }

        @ViewBuilder
        var fallYear: some View {
            if let year = landing.year {
                HStack(spacing: 0) {
                    Text(landing.fall.rawValue + " ")
                    Text(String(year)).highlighted(color: ViewConstants.yearHightColor)
                }
            } else {
                Text(landing.fall.rawValue)
            }
        }

        @ViewBuilder
        var landingClass: some View {
                HStack(spacing: 0) {
                    Text("Class: ")
                    Text(String(landing.recclass)).highlighted(color: ViewConstants.recclassHighlightColor)
                }
        }
    }
}

// MARK: - Cluster landing
extension MeteoriteLandingRowView {
    struct Cluster: View {
        let cluster: LandingCluster

        var body: some View {
            VStack(alignment: .leading, spacing: ViewConstants.spacingBetweenLines) {
                title

                HStack {
                    yearView
                    totalMass
                    averageMass
                }
                .font(.caption.bold())
            }
        }

        var title: some View {
            Text(cluster.name)
                .font(.headline) +
            Text(" (cluster)")
                .fontWeight(.light)
        }

        var yearView: some View {
            cluster.year
                .flatMap { Text(String($0)).highlighted(color: ViewConstants.yearHightColor) }
        }

        var totalMass: some View {
            cluster.totalMass
                .flatMap { total in
                    HStack(spacing: 0) {
                        Text("total: ")
                        Text(total.formatted(MassFormatStyle()))
                            .highlighted(color: ViewConstants.totalMassHighlightColor)
                    }
                }
        }

        var averageMass: some View {
            cluster.averageMass
                .flatMap { average in
                    HStack(spacing: 0) {
                        Text("average: ")
                        Text(average.formatted(MassFormatStyle()))
                            .highlighted(color: ViewConstants.averageMassHighlightColor)
                    }
                }
        }
    }
}

// MARK: - Text.highlighted
private extension Text {
    func highlighted(color: Color) -> some View {
            self
            .padding(MeteoriteLandingRowView.ViewConstants.highlightPadding)
            .background(
                RoundedRectangle(cornerRadius: MeteoriteLandingRowView.ViewConstants.highlightCornerRadius)
                    .foregroundColor(color)
            )
    }
}

// MARK: - Preview
struct MeteoriteLandingRowView_Previews: PreviewProvider {
    static var previews: some View {
//        MeteoriteLandingRowView(landing: Landing.example as MeteoriteLanding)
        MeteoriteLandingRowView(landing: LandingCluster.example as MeteoriteLanding)
            .padding()
            .preferredColorScheme(.dark)
    }
}
