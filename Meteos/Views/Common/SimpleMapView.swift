//
//  SimpleMapView.swift
//  Meteos
//
//  Created by Michal on 25.06.22.
//

import SwiftUI
import MapKit

// MARK: - Constants
private extension SimpleMapView {
    enum Constants {
        static let initialSpanForSingleLanding = MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)
    }
}

struct SimpleMapView: View {
    // MARK: - Properties & States
    private let landingKeys: [MeteoriteLandingID]
    private let landings: [MeteoriteLandingID: MeteoriteLanding]
    private var location: Location

    @State private var mapRegion: MKCoordinateRegion

    // MARK: - Init
    init?(landings: [MeteoriteLanding]) {
        guard let location = landings.first?.location else {
            return nil
        }
        self.location = location
        self.landings = landings.asDictionary()
        self.landingKeys = landings
            .reduce(into: [MeteoriteLandingID]()) { $0.append($1.identification)}

        self.mapRegion = MKCoordinateRegion(center: location, span: Constants.initialSpanForSingleLanding)
    }

    // MARK: - Body
    var body: some View {
        Map(
            coordinateRegion: $mapRegion,
            annotationItems: landingKeys,
            annotationContent: { landingID in
                MapAnnotation(coordinate: location) {
                    LandingMapAnnotation(landing: landings[landingID]!)
                }
            })
    }

    // MARK: - Private functions
}

struct SimpleMapView_Previews: PreviewProvider {
    static var previews: some View {
        SimpleMapView(landings: LandingCluster.example.landings)
    }
}
