//
//  LandingsPersistenceManager.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

class LandingsPersistenceManager {
    private var saveFilename = "landings.json"

    func persist(_ landings: [Landing]) throws {
        try saveAsJSONToDocuments(landings, filename: saveFilename)
    }

    func retrieve() throws -> [Landing] {
        return try readJSONFromDocuments(as: [Landing].self, filename: saveFilename)
    }
}
