//
//  LandingStore.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

// MARK: - Protocol
protocol LandingsStoring {
    func getLandings() async throws -> [Landing]
    func getClusters() async throws -> [MeteoriteLanding]
}

// MARK: - Implementation
actor LandingsStore: LandingsStoring {
    private let apiManager: APIManaging
    private let persistenceManager: LandingsPersistenceManager
    private var cache: [Landing]

    private static let lastUpdatedDateSaveKey = "landingsLastUpdated"

    init(
        apiManager: APIManaging? = nil,
        landingsPersistenceManager: LandingsPersistenceManager? = nil
    ) {
        self.apiManager = apiManager ?? APIManager()
        self.persistenceManager = landingsPersistenceManager ?? LandingsPersistenceManager()
        self.cache = []
    }

    // MARK: - Protocol Functions
    func getLandings() async throws -> [Landing] {
        if cache.isNotEmpty {
            return cache
        }
        if isUpToDate,
           let cachedLandings = try? persistenceManager.retrieve() {
            cache = cachedLandings
            return cachedLandings
        }
        return try await fetchLandings()
    }

    func getClusters() async throws -> [MeteoriteLanding] {
        return try await getLandings().asClusters()
    }

    // MARK: - Private Implementation & functions
    private func fetchLandings() async throws -> [Landing] {
        let landings = try await withThrowingTaskGroup(of: Response.self) { group -> Response in
            let currentYear = Calendar.current.component(.year, from: Date())
            var fetchedLandings = Response()
            for year in Constants.firstYear...currentYear {
                group.addTask {
                    let response: Response = try await self.apiManager.request(Router.landings(year: year))
                    return response
                }

                while let response = try await group.next() {
                    fetchedLandings.append(contentsOf: response)
                }
            }
            return fetchedLandings
        }
        try? persistenceManager.persist(landings)
        cache = landings
        UserDefaults.standard.set(
            ISO8601DateFormatter().string(from: Date()),
            forKey: Self.lastUpdatedDateSaveKey
        )
        return landings
    }

    private var isUpToDate: Bool {
        Calendar.current.date(byAdding: .day, value: -1, to: Date())
            .flatMap { yesterday in
                UserDefaults.standard.string(forKey: Self.lastUpdatedDateSaveKey)
                    .flatMap(ISO8601DateFormatter().date)
                    .flatMap {
                        $0 > yesterday
                    }
            }
            .otherwise(false)
    }
}
