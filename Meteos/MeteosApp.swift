//
//  MeteosApp.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import SwiftUI
import DependencyInjection

@main
struct MeteosApp: App {

    init() {
        Container.shared.register(in: .new) { _ in
            LocationManager()
        }
        Container.shared.register(type: LandingsStoring.self, in: .shared) { _ in
            LandingsStore()
        }
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .preferredColorScheme(.dark)
        }
    }
}
