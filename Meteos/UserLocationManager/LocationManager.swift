//
//  UserLocationManager.swift
//  Meteos
//
//  Created by Michal on 21.06.22.
//

import CoreLocation

public typealias Location = CLLocationCoordinate2D

// inspiration: https://betterprogramming.pub/convert-your-swift-facades-to-the-new-async-await-syntax-using-continuations-d4a7bda4611b
// swiftlint:disable:previous line_length
final class LocationManager: NSObject {
    private typealias LocationCheckedThrowingContinuation = CheckedContinuation<Location, Error>

    fileprivate lazy var locationManager = CLLocationManager()

    private var locationCheckedThrowingContinuation: LocationCheckedThrowingContinuation?

    func getLocation() async throws -> Location {
        return try await
            withCheckedThrowingContinuation { [weak self] (continuation: LocationCheckedThrowingContinuation) in
                guard let self = self else {
                    return
                }

                self.locationCheckedThrowingContinuation = continuation

                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let locationObj = locations.last {
            let coord = locationObj.coordinate
            let location = Location(latitude: coord.latitude, longitude: coord.longitude)
            locationCheckedThrowingContinuation?.resume(returning: location)
            locationCheckedThrowingContinuation = nil
        }
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        locationCheckedThrowingContinuation?.resume(throwing: error)
        locationCheckedThrowingContinuation = nil
    }
}
