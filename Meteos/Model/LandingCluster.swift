//
//  LandingCluster.swift
//  Meteos
//
//  Created by Michal on 22.06.22.
//

import Foundation

// MARK: - Struct
struct LandingCluster {
    let name: String
    let landings: [Landing]
    let year: Int?
}

// MARK: - Conveniences
extension LandingCluster {
    var averageMass: Double? {
        landings
            .compactMap { $0.mass }
            .average()
    }

    var totalMass: Double? {
        landings
            .compactMap { $0.mass }
            .reduce(0.0, +)
    }
}

// MARK: - Conformances
extension LandingCluster: MeteoriteLanding {
    /// Calculated as approximation of geographic midpoint
    var location: Location? {
        let longitudes = landings.compactMap { $0.location?.longitude }
        let latitudes = landings.compactMap { $0.location?.latitude }

        guard longitudes.isNotEmpty,
              longitudes.count == latitudes.count else { return nil }

        let longitude = longitudes.average()
        let latitude = latitudes.average()

        guard let longitude = longitude,
              let latitude = latitude else { return nil }

        return Location(latitude: latitude, longitude: longitude)
    }

    /// Average mass
    var mass: Double? {
        averageMass
    }
}

// MARK: - Example
extension LandingCluster {
    static var example: LandingCluster {
        LandingCluster(
            name: "Cluster",
            landings: [.example, .example],
            year: 2022
        )
    }
}
