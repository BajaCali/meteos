//
//  Geolocation+Convenience.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

extension Geolocation {
    var latitude: Double {
        self.coordinates[1]
    }

    var longitude: Double {
        self.coordinates[0]
    }
}
