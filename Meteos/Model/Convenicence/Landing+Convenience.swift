//
//  Landing+Convenience.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation
import CoreLocation

// MARK: - Convenience Properties
extension Landing {
    var year: Int? {
        guard let date = self.date else { return nil }
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.component(.year, from: date)
    }

    var mass: Double? {
        massString.flatMap(Double.init)
    }

    var location: Location? {
        self.geolocation .flatMap {
            Location(latitude: $0.latitude, longitude: $0.longitude)
        }
    }
}

// MARK: - Example
extension Landing {
    static var example: Landing {
        Landing(
            name: "Landing",
            id: 1,
            nametype: .valid,
            recclass: "L5",
            massString: "21",
            fall: .fell,
            date: Date(),
            geolocation: Geolocation(coordinates: [16.644495, 49.2134286]) // [::-1]
        )
    }
}
