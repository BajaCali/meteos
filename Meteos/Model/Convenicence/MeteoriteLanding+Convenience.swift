//
//  MeteoriteLanding+Convenience.swift
//  Meteos
//
//  Created by Michal on 23.06.22.
//

import Foundation
import CoreLocation

extension MeteoriteLanding {
    /// - Returns: Distance in meters.
    func distance(from location: Location) -> Double? {
        self.location
            .flatMap { CLLocation(latitude: $0.latitude, longitude: $0.longitude) }
            .flatMap { clLocation in
                let rootLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
                return rootLocation.distance(from: clLocation)
            }
    }

    var identification: MeteoriteLandingID {
        guard let year = year else {
            return MeteoriteLandingID(name)
        }
        return MeteoriteLandingID("\(name) \(year)")
    }

    var scale: Double {
        guard let mass = self.mass else { return 1 }
        switch mass {
        case ..<ViewConstants.MassThreshold.small.maxMassValue:
            return ViewConstants.MassThreshold.small.scale
        case ..<ViewConstants.MassThreshold.normal.maxMassValue:
            return ViewConstants.MassThreshold.normal.scale
        case ..<ViewConstants.MassThreshold.large.maxMassValue:
            return ViewConstants.MassThreshold.large.scale
        default:
            return ViewConstants.MassThreshold.extraLarge.scale
        }
    }

    func unwrapped<Output>(
        singleLanding: @escaping (Landing) -> Output,
        cluster: @escaping (LandingCluster) -> Output) -> Output {
            if let unwrappedCluster = self as? LandingCluster {
                return cluster(unwrappedCluster)
            }
            return singleLanding(self as! Landing)
            // swiftlint:disable:previous force_cast
    }
}
