//
//  MeteoriteLanding.swift
//  Meteos
//
//  Created by Michal on 22.06.22.
//

import Foundation

// MARK: - Protocol
protocol MeteoriteLanding {
    var name: String { get }
    var year: Int? { get }
    var location: Location? { get }
    var mass: Double? { get }
}

// MARK: - ID
struct MeteoriteLandingID {
    var id: String

    init(_ id: String) {
        self.id = id
    }
}

extension MeteoriteLandingID: Hashable { }
extension MeteoriteLandingID: Identifiable { }
