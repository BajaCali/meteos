//
//  Constants.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

enum Constants {
    static let baseAPIUrl = URL(string: "https://data.nasa.gov/")!
    static let apiToken = "FTn3MvlCO1lHTRsysswTK3d18"
    static let defaultLocation = Location(latitude: 49.7437572, longitude: 15.3386383)
    static let firstYear = 2011
}
