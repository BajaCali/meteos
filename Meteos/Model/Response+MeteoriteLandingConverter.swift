//
//  Response+MeteoriteLandingConverter.swift
//  Meteos
//
//  Created by Michal on 22.06.22.
//

import Foundation

extension Response {
    /// Converts array of Landings to array MeteoriteLandings and clusters landings which have the same name and year.
    /// - Returns: Array of `MeteoriteLanding`s containing both `Landing` and `LandingCluster`.
    func asClusters() -> [MeteoriteLanding] {
        Dictionary(grouping: self) { $0.name.trimLastNumerals() }
            .flatMap { name, landingsWithSameName in
                Dictionary(grouping: landingsWithSameName) { $0.year }
                    .map { year, landingsInTheYear in
                        LandingCluster(name: name, landings: landingsInTheYear, year: year)
                    }
            }
            .map {
                if $0.landings.count == 1,
                   let landing = $0.landings.first {
                    return landing
                }
                return $0
            }
    }
}

extension String {
    func trimLastNumerals() -> String {
        let trimmed = self
            .reversed()
            .drop {
                $0.isNumber
            }
            .reversed()
        return String(trimmed).trimmingCharacters(in: .whitespaces)
    }
}
