//
//  Landing.swift
//  Meteos
//
//  Created by Michal on 20.06.22.
//

import Foundation

// MARK: - Response
typealias Response = [Landing]

// MARK: - Landing
struct Landing: Codable {
    let name: String
    @StringCodable var id: Int
    let nametype: Nametype
    let recclass: String
    let massString: String?
    let fall: Fall
    let date: Date?
    let geolocation: Geolocation?

    enum CodingKeys: String, CodingKey {
        case name, id, nametype, recclass, fall
        case massString = "mass"
        case date = "year"
        case geolocation
    }
}

extension Landing: Identifiable { }
extension Landing: MeteoriteLanding { }

// MARK: - Fall
enum Fall: String, Codable {
    case fell = "Fell"
    case found = "Found"
}

// MARK: - Geolocation
struct Geolocation: Codable {
    let coordinates: [Double]
}

// MARK: - Nametype
enum Nametype: String, Codable {
    case relict = "Relict"
    case valid = "Valid"
}
