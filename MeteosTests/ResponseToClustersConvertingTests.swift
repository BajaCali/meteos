//
//  MeteosTests.swift
//  MeteosTests
//
//  Created by Michal on 22.06.22.
//

import XCTest
import License2Chill
@testable import Meteos

class MeteosTests: XCTestCase {
    private var state: State!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        state = State()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        state = nil
    }

    func testSimpleConverting() throws {
        try given.responseIsLoaded(state: state)
        try when.responseToClustersIsConverted(state: state)
        try then.clustersMatchesExpectedClusters(state: state)
    }

}

// MARK: - State
private typealias State = BehaviorState.Converting

private extension BehaviorState {
    class Converting {
        var response: Response?

        var clusters: [MeteoriteLanding]?
        var expectedClusters: [MeteoriteLanding]?

    }
}

// MARK: - Given
private extension Given {
    func responseIsLoaded(state: State) throws {
        guard let data = Resources.exampleResponse.data(using: .utf8) else {
            throw "failed to create data"
        }

        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        decoder.dateDecodingStrategy = .formatted(formatter)

        state.response = try decoder.decode(Response.self, from: data)

        state.expectedClusters = Resources.exmapleResponseAsClusters
    }
}

// MARK: - When
private extension When {
    func responseToClustersIsConverted(state: State) throws {
        let response = try XCTUnwrap(state.response)

        state.clusters = response.asClusters()
    }
}

// MARK: - Then
private extension Then {
    func clustersMatchesExpectedClusters(state: State) throws {
        var clusters = try XCTUnwrap(state.clusters)
        var expectedClusters = try XCTUnwrap(state.expectedClusters)

        print(clusters)
        print(expectedClusters)
        XCTAssertEqual(expectedClusters.count, clusters.count)

        clusters.sort { $0.year! < $1.year!}
        expectedClusters.sort { $0.year! < $1.year! }

        try expectedClusters.compare(other: clusters)
    }
}

// MARK: - Helpers
private func compareLandings(_ lhs: MeteoriteLanding, _ rhs: MeteoriteLanding) throws {
    guard lhs.name == rhs.name && lhs.year == lhs.year else {
        throw "landings mismatch: \(lhs) vs \(rhs)"
    }

    if let lhsCluster = lhs as? LandingCluster {
        if let rhsCluster = rhs as? LandingCluster {
            try lhsCluster.landings.compare(other: rhsCluster.landings)
        } else {
            throw "cluster x landing: \(lhs) vs \(rhs)"
        }
    }
}

private extension Array where Element: MeteoriteLanding {
    func compare(other: Self) throws {
        guard self.count == other.count else {
            throw "Count does not match: \(self.count) vs \(other.count)"
        }
        for (i, landing) in self.enumerated() {
            try compareLandings(landing, other[i])
        }

    }
}

private extension Array where Element == MeteoriteLanding {
    func compare(other: Self) throws {
        guard self.count == other.count else {
            throw "Count does not match: \(self.count) vs \(other.count)"
        }
        for (i, landing) in self.enumerated() {
            try compareLandings(landing, other[i])
        }

    }
}
