//
//  String+Error.swift
//  MeteosTests
//
//  Created by Michal on 22.06.22.
//

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}
