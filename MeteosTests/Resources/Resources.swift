//
//  Resources.swift
//  MeteosTests
//
//  Created by Michal on 22.06.22.
//

import Foundation
@testable import Meteos

enum Resources { }

extension Resources {
    static let exampleResponse = """
[{"name":"Aachen 1","id":"1","nametype":"Valid","recclass":"L5","mass":"21","fall":"Fell","year":"1880-01-01T00:00:00.000","reclat":"50.775000","reclong":"6.083330","geolocation":{"type":"Point","coordinates":[6.08333,50.775]}}
,{"name":"Aachen 2","id":"2","nametype":"Valid","recclass":"L5","mass":"21","fall":"Fell","year":"1880-01-01T00:00:00.000","reclat":"50.775000","reclong":"6.083330","geolocation":{"type":"Point","coordinates":[6.08333,50.775]}}
,{"name":"Aachen 3","id":"3","nametype":"Valid","recclass":"L5","mass":"21","fall":"Fell","year":"1881-01-01T00:00:00.000","reclat":"50.775000","reclong":"6.083330","geolocation":{"type":"Point","coordinates":[6.08333,50.775]}}
,{"name":"Aachen 4","id":"4","nametype":"Valid","recclass":"L5","mass":"21","fall":"Fell","year":"1882-01-01T00:00:00.000","reclat":"50.775000","reclong":"6.083330","geolocation":{"type":"Point","coordinates":[6.08333,50.775]}}
,{"name":"Aachen 5","id":"5","nametype":"Valid","recclass":"L5","mass":"21","fall":"Fell","year":"1882-01-01T00:00:00.000","reclat":"50.775000","reclong":"6.083330","geolocation":{"type":"Point","coordinates":[6.08333,50.775]}}
,{"name":"Tomakovka","id":"24019","nametype":"Valid","recclass":"LL6","mass":"600","fall":"Fell","year":"1905-01-01T00:00:00.000","reclat":"47.850000","reclong":"34.766670","geolocation":{"type":"Point","coordinates":[34.76667,47.85]}}]
"""

    static var exmapleResponseAsClusters: [MeteoriteLanding] {
        var components = DateComponents()
        components.year = 1880
        let y1880 = Calendar.current.date(from: components)
        components.year = 1881
        let y1881 = Calendar.current.date(from: components)
        components.year = 1882
        let y1882 = Calendar.current.date(from: components)
        components.year = 1905
        let y1905 = Calendar.current.date(from: components)
        return [
            Meteos.Landing(name: "Tomakovka", id: 24019, nametype: Meteos.Nametype.valid, recclass: "LL6", massString: Optional("600"), fall: Meteos.Fall.fell, date: y1905, geolocation: Optional(Meteos.Geolocation(coordinates: [34.76667, 47.85]))),
            Meteos.Landing(name: "Aachen 3", id: 3, nametype: Meteos.Nametype.valid, recclass: "L5", massString: Optional("21"), fall: Meteos.Fall.fell, date: y1881, geolocation: Optional(Meteos.Geolocation(coordinates: [6.08333, 50.775]))),
            Meteos.LandingCluster(name: "Aachen", landings: [
                Meteos.Landing(name: "Aachen 4", id: 4, nametype: Meteos.Nametype.valid, recclass: "L5", massString: Optional("21"), fall: Meteos.Fall.fell, date: y1882, geolocation: Optional(Meteos.Geolocation(coordinates: [6.08333, 50.775]))),
                Meteos.Landing(name: "Aachen 5", id: 5, nametype: Meteos.Nametype.valid, recclass: "L5", massString: Optional("21"), fall: Meteos.Fall.fell, date: y1881, geolocation: Optional(Meteos.Geolocation(coordinates: [6.08333, 50.775])))
            ], year: Optional(1882)),
            Meteos.LandingCluster(name: "Aachen", landings: [
                Meteos.Landing(name: "Aachen 1", id: 1, nametype: Meteos.Nametype.valid, recclass: "L5", massString: Optional("21"), fall: Meteos.Fall.fell, date: y1880, geolocation: Optional(Meteos.Geolocation(coordinates: [6.08333, 50.775]))),
                Meteos.Landing(name: "Aachen 2", id: 2, nametype: Meteos.Nametype.valid, recclass: "L5", massString: Optional("21"), fall: Meteos.Fall.fell, date: y1880, geolocation: Optional(Meteos.Geolocation(coordinates: [6.08333, 50.775])))
            ], year: Optional(1880))
        ]
    }
}
